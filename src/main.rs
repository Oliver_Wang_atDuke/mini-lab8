use clap::{App, Arg};
use csv::{Reader, Writer};
use serde::{Deserialize, Serialize};
use std::error::Error;
use std::fs::OpenOptions;
use std::path::Path;

#[derive(Debug, Deserialize, Serialize)]
struct Record {
    number: i32,
}

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("CSV Manager")
        .version("1.0")
        .author("Your Name")
        .about("Manages a CSV file of numbers")
        .arg(
            Arg::with_name("add")
                .short("a")
                .long("add")
                .value_name("NUMBER")
                .help("Adds a number to the CSV file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("count")
                .short("c")
                .long("count")
                .help("Counts the numbers in the CSV file"),
        )
        .arg(
            Arg::with_name("FILE")
                .help("Sets the CSV file to use")
                .required(true)
                .index(1),
        )
        .get_matches();

    let file_path = matches.value_of("FILE").unwrap();
    if matches.is_present("add") {
        let num: i32 = matches.value_of("add").unwrap().parse()?;
        add_number(file_path, num)?;
    } else if matches.is_present("count") {
        let count = count_numbers(file_path)?;
        println!("Total numbers: {}", count);
    }

    Ok(())
}

fn add_number(file_path: &str, num: i32) -> Result<(), Box<dyn Error>> {
    let path = Path::new(file_path);
    let file = OpenOptions::new()
        .write(true)
        .append(true)
        .create(true)
        .open(path)?;

    let mut wtr = Writer::from_writer(file);
    wtr.serialize(Record { number: num })?;
    wtr.flush()?;
    Ok(())
}

fn count_numbers(file_path: &str) -> Result<usize, Box<dyn Error>> {
    let path = Path::new(file_path);
    let file = OpenOptions::new().read(true).open(path)?;
    let mut rdr = Reader::from_reader(file);

    let count = rdr.deserialize::<Record>().count();
    Ok(count)
}



#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    #[test]
    fn test_add_number() -> Result<(), Box<dyn Error>> {
        let file_path = "test_output`.csv";
        add_number(file_path, 42)?;
        let count = count_numbers(file_path)?;
        assert_eq!(count, 1);
        fs::remove_file(file_path)?; 
        Ok(())
    }

    #[test]
    fn test_count_numbers() -> Result<(), Box<dyn Error>> {
        let file_path = "test_output.csv";
        fs::write(file_path, "number\n1\n2\n3\n")?; // 预先写入一些数据
        let count = count_numbers(file_path)?;
        assert_eq!(count, 3);
        fs::remove_file(file_path)?; 
        Ok(())
    }
}